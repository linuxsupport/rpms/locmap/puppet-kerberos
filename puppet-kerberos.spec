Name:           puppet-kerberos
Version:        2.13
Release:        1%{?dist}
Summary:        Puppet module for kerberos service

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-agent
Obsoletes:      puppet-keytab <= %{version}
Provides:       puppet-keytab = %{version}

%description
Puppet module for kerberos service.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/kerberos/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/kerberos/
touch %{buildroot}/%{_datadir}/puppet/modules/kerberos/linuxsupport

%files -n puppet-kerberos
%{_datadir}/puppet/modules/kerberos
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.13-1
- Add autoreconfigure %post script

* Mon Feb 12 2024 Ben Morrice <ben.morrice@cern.ch> 2.12-1
- ensure authselect is installed before calling it

* Mon Jan 22 2024 Ben Morrice <ben.morrice@cern.ch> 2.11-1
- tweak condition of authselect command due to changed behavour of the
  pam default on RHEL8

* Thu Dec 07 2023 Ben Morrice <ben.morrice@cern.ch> 2.10-1
- deploy krb5 configuration before attempting to generate a keytab

* Thu Oct 05 2023 Ben Morrice <ben.morrice@cern.ch> 2.9-1
- Integrate 'keytab' module into a single class

* Tue Oct 03 2023 Marta Vila Fernandes <marta.vila.fernandes@cern.ch> 2.8-1
- Add the local_kerberos_package and kdc_cern that corresponds to the 'others' domain

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.7-3
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.7-2
- fix requires for puppet-agent

* Thu Feb 11 2021 Ben Morrice <ben.morrice@cern.ch> - 2.7-1
- move away from hiera lookups

* Tue Jan 05 2021 Ben Morrice <ben.morrice@cern.ch> - 2.6-1
- disable KCM ccache on C8 (for EOS and friends)

* Fri Jul 10 2020 Ben Morrice <ben.morrice@cern.ch> - 2.5-1
- INC2472222 add default_tkt_enctypes to krb5.conf on CC7

* Mon Feb 24 2020 Ben Morrice <ben.morrice@cern.ch> - 2.4-2
- restart sssd after installation of cern-sssd-conf

* Mon Feb 10 2020 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- define git kerberos cloning fix

* Tue Jan 28 2020 Ben Morrice <ben.morrice@cern.ch> - 2.2-2
- add logic to define kdc server correctly on C8 for atlas/cms/tn

* Mon Jan 27 2020 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- adapt config for C8
- deploy cern-{krb5,sssd)-conf packages on C8

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- Rebuild for el8

* Wed Sep 05 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.1-1
- Add few FNAL kdcs

* Mon Jun 04 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-2
- Fix an issue with authconfig not being executed for pam settings.

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
