## puppet-kerberos

This kerberos module configures /etc/krb<version>.conf and /root/.k5login files  
*Needs puppet-keytab module.*

Locmap guesses the network domain of the system, helping puppet to find the right kdc server.

### Parameters to kerberos class
*`k5login_values` A list with names that can login as a root to the remote system, default name is the responsible user of each system

uppet databindings allows the above value to be set via hiera, users can override the default values inside '/etc/puppet/userdata/module_names/kerberos/kerberos.yaml'  

```YAML
---
k5login_values:
- username@CERN.CH
- username1@CERN.CH
``` 
*Writing in /etc/puppet/userdata/module_names/kerberos/kerberos.yaml means that locmap behaviour is overriden, dont forget to include ALL the usernames that you want.* 

## Contact
Aris Boutselis <aris.boutselis@cern.ch>  

## Support
https://gitlab.cern.ch/linuxsupport/puppet-kerberos
