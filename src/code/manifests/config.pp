class kerberos::config {

  if (versioncmp($::operatingsystemmajrelease,'8') >= 0) {
    $authconfig_command = '/usr/bin/authselect select sssd with-silent-lastlog --force'
    $authconfig_unless = '/usr/bin/grep -qc sss /etc/pam.d/password-auth'

    $local_kerberos_package = lookup({"name" => "cern_network_domain", "default_value" => 'default'}) ? {
      'atlas'   => 'cern-krb5-conf-atlas',
      'cms'     => 'cern-krb5-conf-cms',
      'tn'      => 'cern-krb5-conf-tn',
      'others'  => 'cern-krb5-conf',
      default   => 'cern-krb5-conf',
    }

    # EOS does not work with KCM, force file kerberos ccache
    file {'/etc/krb5.conf.d/kcm_default_ccache':
      ensure => absent,
    }
    file {'/etc/krb5.conf.d/file_default_ccache':
      ensure  => present,
      content => "[libdefaults]\ndefault_ccache_name = FILE:/tmp/krb5cc_%{uid}",
    }
    $cern_kerberos_packages = [ $local_kerberos_package, 'cern-sssd-conf' ]
    package{ $cern_kerberos_packages:
      ensure => present,
      notify => Service['sssd'],
    }
    service {'sssd':
      enable  => true,
      ensure  => running,
      require => [
        Package['cern-sssd-conf'],
        Exec['authconfig enable krb5/sssd'],
      ],
    }
    # git v2.11+ has a change of behaviour when attempting to clone without
    # passing a username.
    # ref: https://github.com/git/git/blob/master/Documentation/RelNotes/2.11.0.txt#L482-L486
    # Let's set a sane default to ensure that gitlab.cern.ch clones work
    # as expected
    ini_setting { 'set git http emptyAuth':
      ensure  => present,
      path    => '/etc/gitconfig',
      section => 'http',
      setting => 'emptyAuth',
      value   => 'true',
    }
  } else {
    $authconfig_command = '/usr/sbin/authconfig --enablekrb5 --update'
    $authconfig_unless = '/usr/bin/grep -qc krb /etc/pam.d/*'
    file { 'krb5.conf':
      ensure  => present,
      path    => $kerberos::params::krb5_file,
      owner   => $kerberos::params::owner,
      group   => $kerberos::params::group,
      mode    => $kerberos::params::mode,
      content => template($kerberos::params::krb5_template),
    }
  }
  file { '/root/.k5login':
    ensure => present,
    path   => $kerberos::params::k5login_file,
    owner  => $kerberos::params::owner,
    group  => $kerberos::params::group,
    mode   => $kerberos::params::mode,
    content => template($kerberos::params::k5template),
  }
  ensure_packages(['authselect'])
  exec { 'authconfig enable krb5/sssd':
      command => $authconfig_command,
      timeout => 30,
      unless  => $authconfig_unless,
      require => Package['authselect'],
  }
}
