class kerberos::params {
  $krb5default          = 'CERN.CH'
  $krb5ticket_lifetime  = '25h'
  $krb5renew_lifetime   = '120h'
  $krb5forwardable      = true
  $krb5proxiable        = true
  $default_tkt_enctypes = 'arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc'
  $krb5chpw_prompt      = true
  $krb5weakcrypto       = false

  $krb5pam              = {'external'         => true,
                          'krb4_convert'     => false,
                          'krb4_convert_524' => false,
                          'krb4_use_as_req'  => false
  }

  $kdc_cern             = lookup({"name" => "cern_network_domain", "default_value" => 'default'}) ? {
    'atlas'   => 'atlasvdc.cern.ch',
    'cms'     => 'cmsvdc.cern.ch',
    'tn'      => 'tndc.cern.ch',
    'gpn'     => 'cerndc.cern.ch',
    'lcg'     => 'cerndc.cern.ch',
    'others'  => 'cerndc.cern.ch',
    'default' => 'cerndc.cern.ch',
    #not sure..
    #'alice'  => 'cerndc.cern.ch',
    #'lchb'   => 'cerndc.cern.ch',
  }

  $pkinit_pool         = 'DIR:/etc/pki/tls/certs/'
  $pkinit_anchors      = 'DIR:/etc/pki/tls/certs/'
#File specific variables
  $krb5_template       = 'kerberos/krb5.erb'
  $krb5_file           = '/etc/krb5.conf'
  $owner               = 'root'
  $group               = 'root'
  $mode                = '644'
  $k5login_file        = '/root/.k5login'
  $k5template          = 'kerberos/k5login.erb'
  $k5login_values      = lookup({"name" => "k5login_values", "default_value" => ''})
}
